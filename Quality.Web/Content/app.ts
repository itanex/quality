﻿declare var angular: any;

console.log("Hello world");



class ReportController {

	static $inject = ["$scope"];
	constructor(
		private $scope
	) {

		$scope.helloWorld = "Hello World, from Angular!";

	}

}



angular.module("Report", [])
	.controller("ReportController", ReportController);